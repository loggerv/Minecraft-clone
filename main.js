// 
function main() {
	var canvas = document.createElement("canvas");
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;

	// Initialize the GL context
	const gl = canvas.getContext("webgl");

	// Only continue if WebGL is available and working
	if (gl == null)
	{
		alert("Unable to initialize WebGL. Your browser or machine may not support it.");
		return;
	}
	
	// Set Clear color to black fully opaque.
	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	// Clear the color buffer with specified clear color 
	gl.clear(gl.COLOR_BUFFER_BIT);
	document.body.appendChild(canvas);
}
// load main function window onloading 
window.onload = main;
