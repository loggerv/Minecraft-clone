var canvas = document.createElement("canvas");
document.body.appendChild(canvas); 		// Adds the canvas to the body element

function update ()
{
	// update canvas width and height depending on
	// window width and height
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;

	var ctx = canvas.getContext("2d");

	// set brush to this color
	ctx.fillStyle = "red";
	// fill rectangle with given supplied brush color 
	ctx.fillRect(0, 0, canvas.width, canvas.height);

	ctx.fillStyle = "green";
	ctx.fillRect(0, 0, 200, 200);

	// update window frame with update function
	window.requestAnimationFrame(update);
}

// update function window request animation frames
window.requestAnimationFrame(update);
